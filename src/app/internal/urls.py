from django.urls import path
from app.internal.transport.rest.handlers import get_user_info

urlpatterns = [
    path("me/<int:id>/", get_user_info, name="me"),
]
