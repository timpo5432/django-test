from django.conf import settings

from telegram.ext import ApplicationBuilder, CommandHandler, MessageHandler, filters

from app.internal.transport.bot.handlers import handle_error, handlers, set_contact
from config.settings import env


def run() -> None:
    application = ApplicationBuilder().token(settings.TGBOT_TOKEN).build()

    for name, handler in handlers.items():
        application.add_handler(CommandHandler(name, handler))

    application.add_error_handler(handle_error)
    application.add_handler(MessageHandler(filters.CONTACT, set_contact))
    application.run_polling()
