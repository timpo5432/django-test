from telegram import KeyboardButton, ReplyKeyboardMarkup, Update
from telegram.ext import ContextTypes

from app.internal.services.user_service import NoPhoneException, _create_or_update_user, _get_user_info, _set_phone


async def handle_error(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    if type(context.error) == NoPhoneException:
        await context.bot.send_message(
            chat_id=update.effective_chat.id,
            text="Номер телефона не указан",
        )
    print(context.error)


async def start(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    await _create_or_update_user(update.effective_user.id, update.effective_user.username)
    await context.bot.send_message(
        chat_id=update.effective_chat.id, text="OK, я сохранил тебя"
    )


async def me(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    user_info = await _get_user_info(update.effective_user.id)
    await context.bot.send_message(
        chat_id=update.effective_chat.id,
        text=f"Твой id: {user_info['id']}, твой ник: @{user_info['username']}, твой телефон: {user_info['phone']}",
    )


async def set_phone(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    reply_markup = ReplyKeyboardMarkup(
        [[KeyboardButton(text="Отправить контакт", request_contact=True)]], one_time_keyboard=True
    )
    await context.bot.send_message(
        chat_id=update.effective_chat.id,
        text="Отправь контакт, чтобы я мог сохранить твой номер",
        reply_markup=reply_markup,
    )


async def set_contact(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    await _set_phone(update.effective_user.id, update.effective_message.contact.phone_number)
    await context.bot.send_message(chat_id=update.effective_chat.id, text="номер сохранен")


handlers = {
    "start": start,
    "me": me,
    "set_phone": set_phone,
}
