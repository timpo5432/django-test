from asgiref.sync import async_to_sync
from django.http import JsonResponse
from rest_framework import status
from rest_framework.decorators import api_view

from app.internal.services.user_service import NoPhoneException, \
    _get_user_info


@api_view(["GET"])
def get_user_info(request, id):
    try:
        get_user_info_sync = async_to_sync(_get_user_info)
        return JsonResponse(get_user_info_sync(id), status=status.HTTP_200_OK)
    except NoPhoneException:
        return JsonResponse({"error": "Phone not filled"}, status=status.HTTP_400_BAD_REQUEST)
    except ValueError:
        return JsonResponse({"error": "User not found"}, status=status.HTTP_404_NOT_FOUND)

