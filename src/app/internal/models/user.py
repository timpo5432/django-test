from django.db import models


class User(models.Model):
    id = models.IntegerField(primary_key=True)
    username = models.CharField(max_length=128)
    phone = models.CharField(max_length=32, unique=True, null=True, default=None)

    def __str__(self):
        return f"id: {self.id}; username: {self.username}; phone: {self.phone}"
